#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>


void Send_signal(void); //deklaracja zmiennych globalnych i funkcji
void Initialize_external_interrupt (void);
void Initialize_timer1 (void);
int working;
int rising_edge;
int timer_value;
int distance_cm;
int error;

void Send_signal1(void);
void Initialize_external_interrupt0 (void);
void Initialize_timer0 (void);
int working1;
int rising_edge1;
int timer_value1;
int distance_cm1;
int error1;



void silnik1_konfiguruj(void) //funkcja konfiguruj�ca silnik
{
DDRA|= (1<<0);
DDRA|= (1<<1);
DDRA|= (1<<2);
DDRA|= (1<<3);

}

void silnik1_start(void) //funkcja uruchomiaj�ca silniki
{
PORTA|= (1<<0);
PORTA|= (1<<1);

}

void silnik_prosto(void) //silniki ustawione do jazdy na wprost
{
PORTA|= (1<<0);
PORTA|= (1<<1);

}


void silnik2_prosto(void)
{
PORTA|= (1<<0);
PORTA|= (1<<1);

}

void kolko(void) //jeden silnik wy��czony, robot skr�ca
{
PORTA&= ~(1<<1);
PORTA|= (1<<0);
//PORTA|= (1<<2);
}

void kolko2(void)
{
PORTA|= (1<<1);
PORTA&= ~(1<<0);
//PORTA|= (1<<2);
}


void Initialize_external_interrupt()
{
	MCUCR |= (1 << ISC10); //zmiana stanu logicznego na int1 za��cza przerwanie
	GICR |= (1 << INT1); //w��cz INT1
}
void Initialize_external_interrupt0()
{
	MCUCR |= (1 << ISC00); //zmiana stanu logicznego na int1 za��cza przerwanie
	GICR |= (1 << INT0); //w��cz INT1
}

void Initialize_timer1()
{
	TCCR1B |= (1 << CS10); //brak prescalera
	TCNT1 = 0;			//Resetuj timer
	TIMSK |= (1 << TOIE1); //Timer overflow interrupt w��czona 
} 

void Initialize_timer0()
{
	TCCR0 |= (1 << CS00); //brak prescalera
	TCNT0 = 0;			//Resetuj timer
	TIMSK |= (1 << TOIE0); //Timer overflow interrupt w��czona
}


ISR (TIMER1_OVF_vect)
{
	if(rising_edge==1) //sprawdz czy jest  echo
	{
		timer_value++;
		/*sprawdz czy nie jest poza zasiegiem*/
		if(timer_value > 91)
		{
			working = 0;
			rising_edge = 0;
			error = 1;
		}
	}
}

ISR (TIMER0_OVF_vect)
{
	if(rising_edge1==1) //sprawdz czy jest echo 
	{
		timer_value1++;
		/*sprawdz czy nie jest poza zasiegiem*/
		if(timer_value1 > 91)
		{
			working1 = 0;
			rising_edge1 = 0;
			error1 = 1;
		}
	}
}

ISR (INT1_vect)
{
	if(working==1) //je�eli jest echo wystartuj timer
	{
		if(rising_edge==0)
		{
			rising_edge=1;
			TCNT1 = 0;
			timer_value = 0;
		}
		else //brak echa przelicz odleg�o��
		{
			rising_edge = 0;
			distance_cm = (timer_value*256 + TCNT1)/58;
			working = 0;
				
			   
			   			
		}
	}
	
}

ISR (INT0_vect)
{
	if(working1==1) //sprawdz czy jest echo , start timer
	{
		if(rising_edge1==0)
		{
			rising_edge1=1;
			TCNT0 = 0;
			timer_value1 = 0;
		}
		else //brak echa , przelicz dystans
		{
			rising_edge1 = 0;
			distance_cm1 = (timer_value1*256 + TCNT0)/58;
			working1 = 0;
				
			   
			   			
		}
	}
	
}

int main(void)
{
	/*inicjalizacja przerwa� i timerow */
	Initialize_external_interrupt();
	Initialize_external_interrupt0();
	Initialize_timer0();
	Initialize_timer1();

	silnik1_konfiguruj();	
	DDRD &=~ (1 << PIND3);
	DDRB |= (1 << PINB1); //czujniki ustaw na wyjscie
	DDRB |= (1 << PINB0);
	DDRB|= (1<<4); //diody ustaw na wyjscie
	DDRB|= (1<<5);

	
	sei();
    while(1)
	{
	Send_signal();
	Send_signal1();
	//silnik1_start();
	if(distance_cm<=200) // dwie instrukcje warunkowe nastepuj�ce po sobie powoduj�ce 
	                     // zmiane toru jazdy w momencie wykrycia przeszkody w odleg�o�ci mniejszej ni� 200cm

			   {
			    PORTB|= (1<<4);
			
				
				kolko();

                
	           }else
			   {
                PORTB&= ~(1<<4);
				silnik_prosto();
			   }

               if(distance_cm<=200)

			   {
			    PORTB|= (1<<5);
			
				
				kolko2();

                
	           }else
			   {
                PORTB&= ~(1<<5);
				silnik2_prosto();
			   }
	
    }
}
/* dwie funkcje wysy�aj�ce sygna� z czujnika*/
void Send_signal() 
{
	if(working ==0) //sprawdz czy konwersja jest zako�czona
	{		//restart czujnika
	PORTB |= (1 << PINB1); //wyslij 10us impuls
	_delay_us(10);
	PORTB &=~ (1 << PINB1);
	working = 1;	
	error = 0;		//wyczy�� b��dy
	}	
}
void Send_signal1()
{
	if(working1 ==0) //sprawdz czy konwersja jest zako�czona
	{		//restart czujnika
	PORTB |= (1 << PINB0); //wy�lij 10us impuls
	_delay_us(10);
	PORTB &=~ (1 << PINB0);
	working1 = 1;	
	error1 = 0;		//wyczy�� b��dy
	}	
}
